package com.example.ormikopo.cs_akazoo_app.tracks.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ormikopo.cs_akazoo_app.R;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistUI;

public class TracksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracks);

        //String playlistId = getIntent().getStringExtra("playlistId");
        PlaylistUI playlist = getIntent().getParcelableExtra("playlistUI");

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.tracks_root, TracksFragment.newInstance(playlist))
                .commit();
    }
}