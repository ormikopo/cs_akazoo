package com.example.ormikopo.cs_akazoo_app.tracks.data;

import android.content.Context;
import android.os.AsyncTask;

import com.example.ormikopo.cs_akazoo_app.base.AkazooApplicationDatabase;
import com.example.ormikopo.cs_akazoo_app.rest.RestClient;
import com.example.ormikopo.cs_akazoo_app.rest.responses.TracksResponse;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.Track;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TrackDomain;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TracksInteractor;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class TracksInteractorImpl implements TracksInteractor {

    @Override
    public void getTracks(final Context context, final String playlistId, final OnTracksFinishListener listener) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                // First fetch from the DB
                final TrackDAO tracksDao = AkazooApplicationDatabase.getDatabase(context).trackDAO();

                final List<Track> tracksFromDb = tracksDao.loadTracksOfPlaylist(playlistId);

                if(tracksFromDb != null && tracksFromDb.size() > 0) {

                    ArrayList<TrackDomain> result = new ArrayList<>();

                    for (Track trackFromDb : tracksFromDb) {
                        result.add(new TrackDomain(trackFromDb.getTrackName(), trackFromDb.getTrackArtist(), trackFromDb.getTrackLogoUrl(), trackFromDb.getTrackCategory()));
                    }

                    listener.OnSuccess(result);

                }
                // If nothing in DB then go to network
                else {

                    Call<TracksResponse> call = RestClient.call().fetchPlaylistTracks(playlistId);

                    call.enqueue(new Callback<TracksResponse>() {

                        @Override
                        public void onResponse(Call<TracksResponse> call, final Response<TracksResponse> response) {
                            try {

                                // Save in the DB for caching
                                AsyncTask.execute(new Runnable() {

                                    @Override
                                    public void run() {

                                        List<Track> tracksToAddInDb = new ArrayList<>();

                                        for (TrackDomain trackFromNetwork : response.body().getResult().getItems()) {
                                            tracksToAddInDb.add(new Track(trackFromNetwork.getTrackName(), trackFromNetwork.getTrackArtist(), trackFromNetwork.getTrackLogoUrl(), trackFromNetwork.getTrackCategory(), playlistId));
                                        }

                                        tracksDao.insertTracks(tracksToAddInDb.toArray(new Track[tracksToAddInDb.size()]));

                                        listener.OnSuccess(response.body().getResult().getItems());
                                    }

                                });

                            } catch (Exception e) {
                                onFailure(call, e);
                            }
                        }

                        @Override
                        public void onFailure(Call<TracksResponse> call, Throwable t) {
                            Timber.e("Failed to fetch tracks of playlist from the server");
                            listener.OnError();
                        }

                    });
                }

            }

        });

    }

    @Override
    public void getFilteredTracks(final String playlistId, final String filter, final OnTracksFinishListener listener) {

        Call<TracksResponse> call = RestClient.call().fetchPlaylistTracks(playlistId);

        call.enqueue(new Callback<TracksResponse>() {
            @Override
            public void onResponse(Call<TracksResponse> call, Response<TracksResponse> response) {

                Timber.e("Got response from akazoo.");

                ArrayList<TrackDomain> filteredTracks = new ArrayList<>();

                ArrayList<TrackDomain> tracks = response.body().getResult().getItems();

                for (int i = 0; i < tracks.size(); i++) {
                    if (tracks.get(i).getTrackName().startsWith(filter)) {
                        filteredTracks.add(tracks.get(i));
                    }
                }

                listener.OnSuccess(filteredTracks);

            }

            @Override
            public void onFailure(Call<TracksResponse> call, Throwable t) {
                Timber.e("Got error from akazoo.");

                listener.OnError();
            }
        });
    }

}