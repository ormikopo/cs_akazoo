package com.example.ormikopo.cs_akazoo_app.playlists.domain;

import android.content.Context;

import java.util.ArrayList;

public interface PlaylistsInteractor {

    void getPlaylists(OnPlaylistsFinishListener listener, Context context);

    void getFilteredPlaylists(String filter, OnPlaylistsFinishListener listener);

    interface OnPlaylistsFinishListener {

        void OnSuccess(ArrayList<PlaylistDomain> playlists);

        void OnError();

    }

}