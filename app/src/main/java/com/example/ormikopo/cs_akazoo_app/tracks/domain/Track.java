package com.example.ormikopo.cs_akazoo_app.tracks.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.ormikopo.cs_akazoo_app.playlists.domain.Playlist;

@Entity(
        tableName = "tracks",
        indices = {@Index("artist_name"), @Index("album_name"), @Index("playlist_id")},
        foreignKeys = @ForeignKey(entity = Playlist.class,
                parentColumns = "playlistId",
                childColumns = "playlist_id")
)
public class Track {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int trackId;

    @ColumnInfo(name = "track_name")
    private String trackName;

    @ColumnInfo(name = "artist_name")
    private String trackArtist;

    @ColumnInfo(name = "image_url")
    private String trackLogoUrl;

    @ColumnInfo(name = "album_name")
    private String trackCategory;

    @ColumnInfo(name = "playlist_id")
    private String playlistId;

    public Track(String trackName, String trackArtist, String trackLogoUrl, String trackCategory, String playlistId) {
        this.trackName = trackName;
        this.trackArtist = trackArtist;
        this.trackLogoUrl = trackLogoUrl;
        this.trackCategory = trackCategory;
        this.playlistId = playlistId;
    }

    @NonNull
    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(@NonNull int trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackArtist() {
        return trackArtist;
    }

    public void setTrackArtist(String trackArtist) {
        this.trackArtist = trackArtist;
    }

    public String getTrackLogoUrl() {
        return trackLogoUrl;
    }

    public void setTrackLogoUrl(String trackLogoUrl) {
        this.trackLogoUrl = trackLogoUrl;
    }

    public String getTrackCategory() {
        return trackCategory;
    }

    public void setTrackCategory(String trackCategory) {
        this.trackCategory = trackCategory;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }
}