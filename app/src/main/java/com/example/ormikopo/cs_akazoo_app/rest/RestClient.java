package com.example.ormikopo.cs_akazoo_app.rest;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static RestApi REST_API;

    static {
        setupRestClient();
    }

    public static RestApi call() {
        return REST_API;
    }

    private static void setupRestClient() {

        OkHttpClient mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://akazoo.com/services/Test/TestMobileService.svc/")
                .client(mOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST_API = retrofit.create(RestApi.class);

    }

}