package com.example.ormikopo.cs_akazoo_app.playlists.data;

import android.content.Context;
import android.os.AsyncTask;

import com.example.ormikopo.cs_akazoo_app.base.AkazooApplicationDatabase;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.Playlist;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistDomain;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistsInteractor;
import com.example.ormikopo.cs_akazoo_app.rest.RestClient;
import com.example.ormikopo.cs_akazoo_app.rest.responses.PlaylistsResponse;

import org.jetbrains.annotations.Async;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class PlaylistsInteractorImpl implements PlaylistsInteractor {

    @Override
    public void getPlaylists(final OnPlaylistsFinishListener listener, final Context context) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                // First fetch from the DB
                final PlaylistDAO playlistsDao = AkazooApplicationDatabase.getDatabase(context).playlistDao();

                final List<Playlist> playlistsFromDb = playlistsDao.getAllPlaylists();

                if(playlistsFromDb != null && playlistsFromDb.size() > 0) {
                    ArrayList<PlaylistDomain> result = new ArrayList<>();

                    for (Playlist playlistFromDb : playlistsFromDb) {
                        result.add(new PlaylistDomain(playlistFromDb.getPlaylistId(), playlistFromDb.getName(), playlistFromDb.getDuration(), playlistFromDb.getItemCount(), playlistFromDb.getPhotoUrl()));
                    }

                    listener.OnSuccess(result);
                }
                // If nothing in DB then go to network
                else {
                    Call<PlaylistsResponse> call = RestClient.call().fetchPlaylists();

                    call.enqueue(new Callback<PlaylistsResponse>() {

                        @Override
                        public void onResponse(Call<PlaylistsResponse> call, final Response<PlaylistsResponse> response) {
                            try {

                                // Save in the DB for caching
                                AsyncTask.execute(new Runnable() {

                                    @Override
                                    public void run() {

                                        List<Playlist> playlistsToAddInDb = new ArrayList<>();

                                        for (PlaylistDomain playlistFromNetwork : response.body().getResult()) {
                                            playlistsToAddInDb.add(new Playlist(playlistFromNetwork.getPlaylistId(), playlistFromNetwork.getName(), playlistFromNetwork.getDuration(), playlistFromNetwork.getItemCount(), playlistFromNetwork.getPhotoUrl()));
                                        }

                                        playlistsDao.insertPlaylists(playlistsToAddInDb.toArray(new Playlist[playlistsToAddInDb.size()]));

                                        listener.OnSuccess(response.body().getResult());
                                    }

                                });


                            } catch (Exception e) {
                                onFailure(call, e);
                            }
                        }

                        @Override
                        public void onFailure(Call<PlaylistsResponse> call, Throwable t) {
                            Timber.e("Failed to fetch playlists from the server");
                            listener.OnError();
                        }

                    });
                }
            }

        });

    }

    @Override
    public void getFilteredPlaylists(final String filter, final OnPlaylistsFinishListener listener) {

        Call<PlaylistsResponse> call = RestClient.call().fetchPlaylists();

        call.enqueue(new Callback<PlaylistsResponse>() {
            @Override
            public void onResponse(Call<PlaylistsResponse> call, Response<PlaylistsResponse> response) {

                ArrayList<PlaylistDomain> filteredPlaylists = new ArrayList<>();

                ArrayList<PlaylistDomain> playlists = response.body().getResult();
                for (int i = 0; i < playlists.size(); i++) {
                    if (playlists.get(i).getName().startsWith(filter)) {
                        filteredPlaylists.add(playlists.get(i));
                    }
                }

                listener.OnSuccess(filteredPlaylists);

            }

            @Override
            public void onFailure(Call<PlaylistsResponse> call, Throwable t) {
                listener.OnError();
            }
        });

    }

}