package com.example.ormikopo.cs_akazoo_app.di;

import com.example.ormikopo.cs_akazoo_app.playlists.presentation.PlaylistsFragment;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistsInteractor;
import com.example.ormikopo.cs_akazoo_app.playlists.data.PlaylistsInteractorImpl;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistsPresenter;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistsView;

import dagger.Module;
import dagger.Provides;

@Module
public class PlaylistsModule {

    @Provides
    PlaylistsPresenter providePlaylistsPresenter(PlaylistsView playlistsView, PlaylistsInteractor playlistsInteractor) {
        //return new PlaylistsPresenterImpl(playlistsView, playlistsInteractor);
        return null;
    }

    @Provides
    PlaylistsView providePlaylistsView() {
        return new PlaylistsFragment();
    }

    @Provides
    PlaylistsFragment providePlaylistsFragment() {
        return new PlaylistsFragment();
    }

    @Provides
    PlaylistsInteractor providePlaylistsInteractor() {
        return new PlaylistsInteractorImpl();
    }

}
