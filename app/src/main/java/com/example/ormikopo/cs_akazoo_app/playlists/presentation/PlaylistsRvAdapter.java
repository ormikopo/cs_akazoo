package com.example.ormikopo.cs_akazoo_app.playlists.presentation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ormikopo.cs_akazoo_app.R;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.OnPlaylistClickListener;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistUI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PlaylistsRvAdapter extends RecyclerView.Adapter<PlaylistsRvAdapter.PlaylistsViewHolder> {

    private ArrayList<PlaylistUI> playlists;
    private OnPlaylistClickListener listener;
    private Context context;

    public PlaylistsRvAdapter(ArrayList<PlaylistUI> playlists, OnPlaylistClickListener listener, Context context) {
        this.playlists = playlists;
        this.listener = listener;
        this.context = context;
    }

    public static class PlaylistsViewHolder extends RecyclerView.ViewHolder {

        TextView mPlaylistName;
        TextView mTracksNumber;
        ImageView mPlaylistLogo;
        RelativeLayout mPlaylistItemRoot;

        public PlaylistsViewHolder(View v) {

            super(v);

            mPlaylistName = v.findViewById(R.id.playlist_name);
            mTracksNumber = v.findViewById(R.id.playlist_track_count);
            mPlaylistLogo = v.findViewById(R.id.playlist_logo);
            mPlaylistItemRoot = v.findViewById(R.id.playlist_item_root);

        }
    }

    @NonNull
    @Override
    public PlaylistsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater
                    .from(viewGroup.getContext())
                    .inflate(R.layout.view_playlist_item, viewGroup, false);

        PlaylistsViewHolder vh = new PlaylistsViewHolder(v);

        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistsViewHolder holder, int position) {

        final PlaylistUI playlist = playlists.get(position);

        holder.mPlaylistName.setTextColor(context.getResources().getColor((playlist.getColorId())));

        holder.mPlaylistName.setText(playlist.getName());
        holder.mTracksNumber.setText(String.valueOf(playlist.getItemCount()));

        Picasso.get()
                .load(playlist.getPhotoUrl())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.mPlaylistLogo);

        holder.mPlaylistLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPlaylistLogoClicked(playlist);
            }
        });

        holder.mPlaylistItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPlaylistClicked(playlist);
            }
        });
    }

    @Override
    public int getItemCount() {
        return playlists.size();
    }
}