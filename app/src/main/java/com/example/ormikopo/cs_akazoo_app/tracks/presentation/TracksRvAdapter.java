package com.example.ormikopo.cs_akazoo_app.tracks.presentation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ormikopo.cs_akazoo_app.R;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.OnTrackClickListener;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TrackUI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TracksRvAdapter extends RecyclerView.Adapter<TracksRvAdapter.TracksViewHolder> {

    private ArrayList<TrackUI> tracks;
    private OnTrackClickListener listener;
    private Context context;

    public TracksRvAdapter(ArrayList<TrackUI> tracks, OnTrackClickListener listener, Context context) {
        this.tracks = tracks;
        this.listener = listener;
        this.context = context;
    }

    public static class TracksViewHolder extends RecyclerView.ViewHolder {

        TextView mTrackName;
        TextView mTrackArtist;
        TextView mTrackCategory;
        ImageView mTrackLogo;
        RelativeLayout mTrackItemRoot;

        public TracksViewHolder(View v) {

            super(v);

            mTrackName = v.findViewById(R.id.track_name);
            mTrackArtist = v.findViewById(R.id.track_artist);
            mTrackCategory = v.findViewById(R.id.track_category);
            mTrackLogo = v.findViewById(R.id.track_logo);
            mTrackItemRoot = v.findViewById(R.id.track_item_root);

        }

    }

    @NonNull
    @Override
    public TracksViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.view_track_item, viewGroup, false);

        TracksViewHolder vh = new TracksViewHolder(v);

        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull TracksViewHolder holder, int position) {

        final TrackUI track = tracks.get(position);

        holder.mTrackName.setTextColor(context.getResources().getColor((track.getColorId())));
        holder.mTrackName.setText(track.getTrackName());
        holder.mTrackArtist.setText(track.getTrackArtist());
        holder.mTrackCategory.setText(track.getTrackCategory());

        Picasso.get()
                .load(track.getTrackLogoUrl())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.mTrackLogo);

        holder.mTrackLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTrackLogoClicked(track);
            }
        });

        holder.mTrackItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTrackClicked(track);
            }
        });

    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }

}