package com.example.ormikopo.cs_akazoo_app.di;

import com.example.ormikopo.cs_akazoo_app.base.AkazooApplication;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;

/**
 * Injects application dependencies.
 */
@Singleton
@Component(modules = AkazooApplicationModule.class)
interface AkazooApplicationComponent extends AndroidInjector<AkazooApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<AkazooApplication> {
    }
}