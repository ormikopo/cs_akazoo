package com.example.ormikopo.cs_akazoo_app.base;

import android.app.Activity;
import android.app.Application;

//import com.example.ormikopo.cs_akazoo_app.di.DaggerAkazooApplicationComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

import timber.log.Timber;

/**
 * The Android {@link Application}.
 * <p>
 * <b>DEPENDENCY INJECTION</b>
 * We could extend {@link dagger.android.support.DaggerApplication} so we can get the boilerplate
 * dagger code for free. However, we want to avoid inheritance (if possible and it is in this case)
 * so that we have to option to inherit from something else later on if needed
 */
public class AkazooApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    public AkazooApplication() {}

    @Override
    public void onCreate() {
        super.onCreate();

        //DaggerAkazooApplicationComponent.builder().create(this).inject(this);

        Timber.plant(new Timber.DebugTree());

        Timber.d("AkazooApplication got created.");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        Timber.d("AkazooApplication got terminated.");
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }
}