package com.example.ormikopo.cs_akazoo_app.playlists.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(
        tableName = "playlists",
        indices = {@Index("name")}
)
public class Playlist {

    @PrimaryKey
    @NonNull
    private String playlistId;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "duration")
    private int duration;

    @ColumnInfo(name = "item_count")
    private int itemCount;

    @ColumnInfo(name = "photo_url")
    private String photoUrl;

    public Playlist(String playlistId, String name, int duration, int itemCount, String photoUrl) {
        this.playlistId = playlistId;
        this.name = name;
        this.duration = duration;
        this.itemCount = itemCount;
        this.photoUrl = photoUrl;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}