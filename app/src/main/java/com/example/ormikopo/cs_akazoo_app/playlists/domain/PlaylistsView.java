package com.example.ormikopo.cs_akazoo_app.playlists.domain;

import java.util.ArrayList;

public interface PlaylistsView {

    void showPlaylists(ArrayList<PlaylistUI> playlists);

    void showGeneralError();

}