package com.example.ormikopo.cs_akazoo_app.playlists.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.ormikopo.cs_akazoo_app.playlists.domain.Playlist;

import java.util.List;

@Dao
public interface PlaylistDAO {

    @Query("SELECT * FROM playlists")
    List<Playlist> getAllPlaylists();

    @Query("SELECT * FROM playlists WHERE playlistId IN (:playlistIds)")
    List<Playlist> loadAllPlaylistsByIds(String[] playlistIds);

    @Query("SELECT * FROM playlists WHERE item_count > :minTrackCount")
    List<Playlist> loadAllPlaylistsWithTracksCountGreaterThan(int minTrackCount);

    @Query("SELECT * FROM playlists WHERE name LIKE :playlistName LIMIT 1")
    Playlist findPlaylistByName(String playlistName);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPlaylist(Playlist playlist);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPlaylists(Playlist... playlists);

    @Update
    void updatePlaylist(Playlist playlist);

    @Update
    void updatePlaylists(Playlist... playlists);

    @Delete
    void deletePlaylist(Playlist playlist);

    @Delete
    void deletePlaylists(Playlist... playlists);

    @Query("DELETE FROM playlists")
    void deleteAllPlaylists();

}