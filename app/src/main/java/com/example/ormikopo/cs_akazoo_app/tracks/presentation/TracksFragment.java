package com.example.ormikopo.cs_akazoo_app.tracks.presentation;


import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ormikopo.cs_akazoo_app.R;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistUI;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.OnTrackClickListener;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TrackUI;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TracksPresenter;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TracksView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class TracksFragment extends Fragment implements TracksView {

    TracksPresenter presenter;

    @BindView(R.id.playlist_name)
    TextView mPlaylistName;

    @BindView(R.id.tracks_rv)
    RecyclerView tracksRv;

    @BindView(R.id.filter_edit_text)
    EditText mFilterEditText; // view properties with m in front -> no m means usual class property

    public TracksFragment() {
        // Required empty public constructor
    }

    public static TracksFragment newInstance(PlaylistUI playlist) {

        TracksFragment mFragment = new TracksFragment();

        Bundle args = new Bundle(); // this will stay in memory even if the fragment is destroyed - the fragment will get the playlist from this bundle
        args.putParcelable("playlist", playlist);
        mFragment.setArguments(args);

        return mFragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        PlaylistUI playlist = getArguments().getParcelable("playlist");

        View view = inflater.inflate(R.layout.fragment_tracks, container, false);

        ButterKnife.bind(this, view);

        mPlaylistName.setText(playlist.getName());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        // assign more options in layoutManager
        //layoutManager.setStackFromEnd(true); // reverse order

        tracksRv.setLayoutManager(layoutManager);

        presenter = new TracksPresenterImpl(this);

        presenter.getTracks(getActivity(), playlist.getPlaylistId());

        return view;

    }

    @Override
    //@UiThread // TODO - See why this does not work
    public void showTracks(final ArrayList<TrackUI> tracks) {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                tracksRv.setAdapter(new TracksRvAdapter(tracks, new OnTrackClickListener() {

                    @Override
                    public void onTrackClicked(TrackUI track) {
                        Toast.makeText(getActivity(), "track '" + track.getTrackName() + "' got clicked", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onTrackLogoClicked(TrackUI track) {
                        Toast.makeText(getActivity(), "the logo of track '" + track.getTrackName() + "' got clicked", Toast.LENGTH_LONG).show();
                    }

                }, getActivity()));

            }

        });

    }

    @Override
    public void showGeneralError() {
        Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.filter_button)
    public void filterTracks(View view) {

        PlaylistUI playlist = getArguments().getParcelable("playlist");

        String filterText = mFilterEditText.getText().toString();

        presenter.getFilteredTracks(playlist.getPlaylistId(), filterText);

    }
}