package com.example.ormikopo.cs_akazoo_app.tracks.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.ormikopo.cs_akazoo_app.tracks.domain.Track;

import java.util.List;

@Dao
public interface TrackDAO {

    @Query("SELECT * FROM tracks")
    List<Track> getAllTracks();

    @Query("SELECT * FROM tracks WHERE playlist_id = :playlistId")
    List<Track> loadTracksOfPlaylist(String playlistId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTrack(Track track);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTracks(Track... tracks);

    @Update
    void updateTrack(Track track);

    @Update
    void updateTracks(Track... tracks);

    @Delete
    void deleteTrack(Track track);

    @Delete
    void deleteTracks(Track... track);

    @Query("DELETE FROM tracks")
    void deleteAllTracks();

}