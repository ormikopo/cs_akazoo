package com.example.ormikopo.cs_akazoo_app.tracks.presentation;

import android.content.Context;

import com.example.ormikopo.cs_akazoo_app.R;
import com.example.ormikopo.cs_akazoo_app.tracks.data.TracksInteractorImpl;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TrackDomain;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TrackUI;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TracksInteractor;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TracksPresenter;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.TracksView;

import java.util.ArrayList;

public class TracksPresenterImpl implements TracksPresenter, TracksInteractor.OnTracksFinishListener {

    TracksView tracksView;
    TracksInteractor interactor;

    public TracksPresenterImpl(TracksView tracksView) {
        this.tracksView = tracksView;
        this.interactor = new TracksInteractorImpl();
    }

    @Override
    public void getTracks(Context context, String playlistId) {
        this.interactor.getTracks(context, playlistId, this);
    }

    @Override
    public void getFilteredTracks(String playlistId, String filter) {
        this.interactor.getFilteredTracks(playlistId, filter, this);
    }

    @Override
    public void OnSuccess(ArrayList<TrackDomain> tracks) {

        ArrayList<TrackUI> trackUis = new ArrayList<TrackUI>();

        for(TrackDomain track : tracks) {

            TrackUI trackUi = new TrackUI(
                    track.getTrackName(),
                    track.getTrackArtist(),
                    track.getTrackCategory(),
                    track.getTrackLogoUrl()
            );

            // Presenter logic here
            if(trackUi.getTrackCategory().equals("Rock")) {
                trackUi.setColorId(R.color.red);
            }
            else if(trackUi.getTrackCategory().equals("Pop")) {
                trackUi.setColorId(R.color.blue);
            }
            else if(trackUi.getTrackCategory().equals("Metal")) {
                trackUi.setColorId(R.color.colorAccent);
            }
            else {
                trackUi.setColorId(R.color.colorPrimary);
            }

            trackUis.add(trackUi);
        }

        tracksView.showTracks(trackUis);
    }

    @Override
    public void OnError() {
        tracksView.showGeneralError();
    }
}