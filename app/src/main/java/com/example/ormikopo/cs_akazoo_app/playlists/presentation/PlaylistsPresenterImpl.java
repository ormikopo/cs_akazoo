package com.example.ormikopo.cs_akazoo_app.playlists.presentation;

import android.content.Context;

import com.example.ormikopo.cs_akazoo_app.R;
import com.example.ormikopo.cs_akazoo_app.playlists.data.PlaylistsInteractorImpl;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistDomain;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistUI;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistsInteractor;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistsPresenter;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistsView;

import java.util.ArrayList;

public class PlaylistsPresenterImpl implements PlaylistsPresenter, PlaylistsInteractor.OnPlaylistsFinishListener {

    PlaylistsView playlistsView;
    PlaylistsInteractor interactor;

    public PlaylistsPresenterImpl(PlaylistsView playlistsView) {
        this.playlistsView = playlistsView;
        this.interactor = new PlaylistsInteractorImpl();
    }

    @Override
    public void getPlaylists(Context context) {
        this.interactor.getPlaylists(this, context);
    }

    @Override
    public void getFilteredPlaylists(String filter) {
        this.interactor.getFilteredPlaylists(filter, this);
    }

    @Override
    public void OnSuccess(ArrayList<PlaylistDomain> playlists) {

        ArrayList<PlaylistUI> playlistsUI = new ArrayList<>();

        if (playlists != null && !playlists.isEmpty()) {

            for (PlaylistDomain playlist : playlists) {
                PlaylistUI playlistUI = new PlaylistUI(
                        playlist.getPlaylistId(),
                        playlist.getName(),
                        playlist.getItemCount(),
                        playlist.getPhotoUrl()
                );

                if (playlistUI.getItemCount() > 10) {
                    playlistUI.setColorId(R.color.red);
                }
                else {
                    playlistUI.setColorId(R.color.blue);
                }

                playlistsUI.add(playlistUI);
            }

        }

        playlistsView.showPlaylists(playlistsUI);

    }

    @Override
    public void OnError() {
        playlistsView.showGeneralError();
    }
}