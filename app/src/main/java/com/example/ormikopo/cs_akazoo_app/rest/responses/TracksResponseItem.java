package com.example.ormikopo.cs_akazoo_app.rest.responses;

import com.example.ormikopo.cs_akazoo_app.tracks.domain.TrackDomain;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TracksResponseItem {

    @SerializedName("Items")
    private ArrayList<TrackDomain> items;

    public TracksResponseItem(ArrayList<TrackDomain> items) {
        this.items = items;
    }

    public ArrayList<TrackDomain> getItems() {
        return items;
    }

    public void setItems(ArrayList<TrackDomain> items) {
        this.items = items;
    }
}