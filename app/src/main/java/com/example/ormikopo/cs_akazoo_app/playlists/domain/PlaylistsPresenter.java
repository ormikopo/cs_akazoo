package com.example.ormikopo.cs_akazoo_app.playlists.domain;

import android.content.Context;

public interface PlaylistsPresenter {

    void getPlaylists(Context context);

    void getFilteredPlaylists(String filter);

}