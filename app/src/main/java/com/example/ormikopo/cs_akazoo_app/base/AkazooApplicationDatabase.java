package com.example.ormikopo.cs_akazoo_app.base;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.ormikopo.cs_akazoo_app.playlists.data.PlaylistDAO;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.Playlist;
import com.example.ormikopo.cs_akazoo_app.tracks.data.TrackDAO;
import com.example.ormikopo.cs_akazoo_app.tracks.domain.Track;

@Database(entities = {Playlist.class, Track.class}, version = 1, exportSchema = false)
public abstract class AkazooApplicationDatabase extends RoomDatabase {

    public abstract PlaylistDAO playlistDao();

    public abstract TrackDAO trackDAO();

    private static volatile AkazooApplicationDatabase INSTANCE;

    public static AkazooApplicationDatabase getDatabase(final Context context) {

        if (INSTANCE == null) {

            synchronized (AkazooApplicationDatabase.class) {

                if (INSTANCE == null) {

                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AkazooApplicationDatabase.class, "akazoo_database")
                            //.addCallback(sRoomDatabaseCallback) // for initial seeding of the database
                            //.allowMainThreadQueries() // we do not want this since it can cause a lock on the main UI thread
                            .build();

                }

            }

        }

        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }

            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final PlaylistDAO mDao;

        PopulateDbAsync(AkazooApplicationDatabase db) {
            mDao = db.playlistDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {

            mDao.deleteAllPlaylists();

            Playlist playlist = new Playlist("PlaylistId1", "Playlist 1 Name", 120, 12, "https://picsum.photos/1000/400");
            mDao.insertPlaylist(playlist);

            playlist = new Playlist("PlaylistId2", "Playlist 2 Name", 180, 15, "https://picsum.photos/1000/400");
            mDao.insertPlaylist(playlist);

            return null;
        }
    }

}