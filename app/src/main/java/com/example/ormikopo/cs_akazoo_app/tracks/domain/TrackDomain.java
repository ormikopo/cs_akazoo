package com.example.ormikopo.cs_akazoo_app.tracks.domain;

import com.google.gson.annotations.SerializedName;

public class TrackDomain {

    @SerializedName("TrackName")
    private String trackName;

    @SerializedName("ArtistName")
    private String trackArtist;

    @SerializedName("ImageUrl")
    private String trackLogoUrl;

    @SerializedName("AlbumName")
    private String trackCategory;

    public TrackDomain(String trackName, String trackArtist, String trackLogoUrl, String trackCategory) {
        this.trackName = trackName;
        this.trackArtist = trackArtist;
        this.trackLogoUrl = trackLogoUrl;
        this.trackCategory = trackCategory;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackArtist() {
        return trackArtist;
    }

    public void setTrackArtist(String trackArtist) {
        this.trackArtist = trackArtist;
    }

    public String getTrackLogoUrl() {
        return trackLogoUrl;
    }

    public void setTrackLogoUrl(String trackLogoUrl) {
        this.trackLogoUrl = trackLogoUrl;
    }

    public String getTrackCategory() {
        return trackCategory;
    }

    public void setTrackCategory(String trackCategory) {
        this.trackCategory = trackCategory;
    }
}