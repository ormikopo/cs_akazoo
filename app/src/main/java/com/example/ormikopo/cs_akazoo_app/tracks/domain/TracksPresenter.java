package com.example.ormikopo.cs_akazoo_app.tracks.domain;

import android.content.Context;

public interface TracksPresenter {

    void getTracks(Context context, String playlistId);

    void getFilteredTracks(String playlistId, String filter);

}