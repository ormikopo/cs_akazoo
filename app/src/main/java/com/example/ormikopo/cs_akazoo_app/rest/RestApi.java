package com.example.ormikopo.cs_akazoo_app.rest;

import com.example.ormikopo.cs_akazoo_app.rest.responses.PlaylistsResponse;
import com.example.ormikopo.cs_akazoo_app.rest.responses.TracksResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApi {

    @GET("playlists")
    Call<PlaylistsResponse> fetchPlaylists();

    @GET("playlist")
    Call<TracksResponse> fetchPlaylistTracks(@Query("playlistId") String playlistId);

}