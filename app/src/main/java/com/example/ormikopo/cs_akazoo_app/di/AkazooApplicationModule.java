package com.example.ormikopo.cs_akazoo_app.di;

import android.app.Application;

import com.example.ormikopo.cs_akazoo_app.base.AkazooApplication;
import com.example.ormikopo.cs_akazoo_app.playlists.presentation.PlaylistsActivity;
import com.example.ormikopo.cs_akazoo_app.tracks.presentation.TracksActivity;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Provides application-wide dependencies.
 */
@Module(includes = AndroidSupportInjectionModule.class)
public abstract class AkazooApplicationModule {

    @Binds
    @Singleton
    /*
     * Singleton annotation isn't necessary since Application instance is unique but is here for
     * convention. In general, providing Activity, Fragment, BroadcastReceiver, etc does not require
     * them to be scoped since they are the components being injected and their instance is unique.
     *
     * However, having a scope annotation makes the module easier to read. We wouldn't have to look
     * at what is being provided in order to understand its scope.
     */
    abstract Application application(AkazooApplication app);

    /**
     * Provides the injector for the {@link PlaylistsActivity}, which has access to the dependencies
     * provided by this application instance (singleton scoped objects).
     */
    //@PerActivity
    //@ContributesAndroidInjector(modules = PlaylistsActivity.class)
    //abstract PlaylistsActivity playlistsActivityInjector();

    /**
     * Provides the injector for the {@link TracksActivity}, which has access to the dependencies
     * provided by this application instance (singleton scoped objects).
     */
    //@PerActivity
    //@ContributesAndroidInjector(modules = TracksActivity.class)
    //abstract TracksActivity tracksActivityInjector();

}