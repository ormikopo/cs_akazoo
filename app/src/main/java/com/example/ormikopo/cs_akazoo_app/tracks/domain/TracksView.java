package com.example.ormikopo.cs_akazoo_app.tracks.domain;

import java.util.ArrayList;

public interface TracksView {

    void showTracks(ArrayList<TrackUI> tracks);

    void showGeneralError();

}