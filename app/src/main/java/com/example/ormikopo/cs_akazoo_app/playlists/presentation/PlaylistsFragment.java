package com.example.ormikopo.cs_akazoo_app.playlists.presentation;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ormikopo.cs_akazoo_app.R;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.OnPlaylistClickListener;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistUI;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistsPresenter;
import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistsView;
import com.example.ormikopo.cs_akazoo_app.tracks.presentation.TracksActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlaylistsFragment extends Fragment implements PlaylistsView {

    PlaylistsPresenter presenter;

    @BindView(R.id.playlists_rv)
    RecyclerView playlistsRv;

    @BindView(R.id.filter_edit_text)
    EditText mFilterEditText; // view properties with m in front -> no m means usual class property

    public PlaylistsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_playlists, container, false);

        ButterKnife.bind(this, view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        // assign more options in layoutManager

        playlistsRv.setLayoutManager(layoutManager);

        presenter = new PlaylistsPresenterImpl(this);

        presenter.getPlaylists(getActivity());

        return view;
    }

    @Override
    //@UiThread // TODO - See why this does not work
    public void showPlaylists(final ArrayList<PlaylistUI> playlists) {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                playlistsRv.setAdapter(new PlaylistsRvAdapter(playlists, new OnPlaylistClickListener() {
                    @Override
                    public void onPlaylistClicked(PlaylistUI playlist) {

                        Intent intent = new Intent(getActivity(), TracksActivity.class);

                        //intent.putExtra("playlistId", playlist.getPlaylistId());
                        intent.putExtra("playlistUI", playlist);

                        getActivity().startActivity(intent);

                    }

                    @Override
                    public void onPlaylistLogoClicked(PlaylistUI playlist) {
                        Toast.makeText(getActivity(), "the logo of playlist '" + playlist.getName() + "' got clicked", Toast.LENGTH_LONG).show();
                    }
                }, getActivity()));

            }

        });

    }

    @Override
    public void showGeneralError() {
        Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.filter_button)
    public void filterPlaylists(View view) {
        String filterText = mFilterEditText.getText().toString();

        presenter.getFilteredPlaylists(filterText);
    }
}