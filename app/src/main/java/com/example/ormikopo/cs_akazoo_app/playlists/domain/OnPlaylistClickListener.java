package com.example.ormikopo.cs_akazoo_app.playlists.domain;

public interface OnPlaylistClickListener {

    void onPlaylistClicked(PlaylistUI playlist);

    void onPlaylistLogoClicked(PlaylistUI playlist);

}