package com.example.ormikopo.cs_akazoo_app.tracks.domain;

import android.content.Context;

import java.util.ArrayList;

public interface TracksInteractor {

    void getTracks(Context context, String playlistId, OnTracksFinishListener listener);

    void getFilteredTracks(String playlistId, String filter, OnTracksFinishListener listener);

    interface OnTracksFinishListener {

        void OnSuccess(ArrayList<TrackDomain> tracks);

        void OnError();

    }

}