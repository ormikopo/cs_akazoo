package com.example.ormikopo.cs_akazoo_app.tracks.domain;

public interface OnTrackClickListener {

    void onTrackClicked(TrackUI track);

    void onTrackLogoClicked(TrackUI track);

}