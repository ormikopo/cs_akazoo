package com.example.ormikopo.cs_akazoo_app.rest.responses;

import com.example.ormikopo.cs_akazoo_app.playlists.domain.PlaylistDomain;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PlaylistsResponse {

    @SerializedName("Result")
    private ArrayList<PlaylistDomain> result;

    public PlaylistsResponse(ArrayList<PlaylistDomain> result) {
        this.result = result;
    }

    public ArrayList<PlaylistDomain> getResult() {
        return this.result;
    }

    public void setResult(ArrayList<PlaylistDomain> result) {
        this.result = result;
    }

}