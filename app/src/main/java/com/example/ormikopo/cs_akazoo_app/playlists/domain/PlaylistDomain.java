package com.example.ormikopo.cs_akazoo_app.playlists.domain;

import com.google.gson.annotations.SerializedName;

public class PlaylistDomain {

    @SerializedName("PlaylistId")
    private String playlistId;

    @SerializedName("Name")
    private String name;

    @SerializedName("Duration")
    private int duration;

    @SerializedName("ItemCount")
    private int itemCount;

    @SerializedName("PhotoUrl")
    private String photoUrl;

    public PlaylistDomain(String playlistId, String name, int duration, int itemCount, String photoUrl) {
        this.playlistId = playlistId;
        this.name = name;
        this.duration = duration;
        this.itemCount = itemCount;
        this.photoUrl = photoUrl;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}