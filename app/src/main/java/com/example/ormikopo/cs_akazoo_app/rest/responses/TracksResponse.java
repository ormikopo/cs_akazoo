package com.example.ormikopo.cs_akazoo_app.rest.responses;

import com.google.gson.annotations.SerializedName;

public class TracksResponse {

    @SerializedName("Result")
    private TracksResponseItem result;

    @SerializedName("ErrorData")
    private String errorData;

    @SerializedName("ErrorId")
    private int errorId;

    @SerializedName("IsError")
    private boolean isError;

    public TracksResponse(TracksResponseItem result, String errorData, int errorId, boolean isError) {
        this.result = result;
        this.errorData = errorData;
        this.errorId = errorId;
        this.isError = isError;
    }

    public TracksResponseItem getResult() {
        return result;
    }

    public void setResult(TracksResponseItem result) {
        this.result = result;
    }

    public String getErrorData() {
        return errorData;
    }

    public void setErrorData(String errorData) {
        this.errorData = errorData;
    }

    public int getErrorId() {
        return errorId;
    }

    public void setErrorId(int errorId) {
        this.errorId = errorId;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }
}